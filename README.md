# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
This is for csci2720 Project in CUHK in 2016 semester 1.


### How do I get set up? ###

* Summary of set up


## 1. Change database to be used by our project ##

-  execute "mongod" in command line

-  execute "mongo" in another command line shell

-  execute "use project"  to change the database

- execute "show collections" to show tables in the database


## 2. Populate Database ##

-  cd to the project directory

-  node initdb.js

## 3. Run the server ##

-  npm start
- Go to localhost:8081


## 4. How to login? ##

-  username: tony / password: tony

-  username: kenny / password: kenny

-  username: javita / password: javita

-  username: yuen / password: yuen


* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### File ###
**index.js**
-  Handles Routing

**db.js**
-  Handles DB connection

**model.js**
-  Store the database schema
-  Handles interaction to database

**initdb.js**
-  Populate sample data in the database

**views**
-  Template files to be returned to the clients
-  "pug" is used as the template engine

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
