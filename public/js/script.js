$(document).ready(function(){
	$("#all").click(function(){
		$("#searchValue").val("");
		$("#searchValue").attr("readonly", true);
		$('[data-toggle="tooltip"]').tooltip("disable");
	});

	$("#username").click(function(){
		$("#searchValue").attr("readonly", null);
		$('[data-toggle="tooltip"]').tooltip('disable');
	});

	$("#tags").click(function(){
		$("#searchValue").attr("readonly", null);
		$("#searchValue").attr("data-toggle", "tooltip");
		$("#searchValue").attr("data-placement", "top");
		$("#searchValue").attr("title", "Use comma to separate if you have multiple tags");
		//data-toggle="tooltip" data-placement="top" title="Use comma to separate four choices"
		$('[data-toggle="tooltip"]').tooltip();
	});

});