// This file contains code to repopulate the DB with test data

var mongoose = require('mongoose');

require('./db.js'); // Set up connection and create DB if it does not exists yet

var model = require('./model.js');
var sha1 = require("sha1");

// Remove existing data from Users and Questions collections and
// repopulate them with test data
model.User.remove({}, function(err) {
  if (err)
    return console.log(err);

  model.Question.remove({}, function(err) {
    if (err)
      return console.log(err);

    populateData();
  });
});


// ----------------------------------------------------------------------


function populateData() {
  var t = ['lv1', 'lv2', 'lv3', 'lv4', 'lv5', 'lv6', 'lv7', 'lv8', 'lv9', 'lv10'];

  var questions = [
    _q(0, 'Question 1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent posuere dui eu lorem condimentum interdum. Sed efficitur justo feugiat maximus tristique. Nullam aliquet nec augue eget tristique. Nam egestas odio quis mollis dapibus. Vestibulum vestibulum tellus ac mi auctor, quis dictum libero viverra. Morbi sagittis, eros eget aliquam molestie, justo erat ullamcorper nulla, id viverra magna felis a tortor. Donec eu lacinia neque. Fusce sed urna non mi convallis pulvinar ut vel dolor. Cras quis placerat felis. Aenean sed sem dui. Suspendisse condimentum in elit vitae efficitur. Nullam vel ultrices dui. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.', 'I am a girl,I am a woman,I am a dog,I am a boy', 4, [t[0], t[1], t[3], t[4]]),
    _q(1, 'Question 2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ut varius ex. Aliquam eleifend turpis ex, non viverra sapien finibus.<img src="https://static.pexels.com/photos/56875/tree-dawn-nature-bucovina-56875.jpeg" width="1000"/>', 'A,B,C,D', 1, [t[3], t[4]]),
    _q(2, 'Question 3', '<u>This</u> <p>is</p> <b>a<b> <i>short</i> <br/>description', '<i>A</i>,<u>B</u>,<p>C</p>,<img src="http://images.freeimages.com/images/premium/large-thumbs/5070/50703258-3d-red-letter-a.jpg"/><pre>D</pre>', 3, [t[5], t[4]]),
    _q(3, 'Question 4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent imperdiet sodales blandit. Morbi elit felis, congue sit amet sem id, mattis pharetra sapien. Morbi sodales mauris ipsum, vel posuere elit ornare facilisis. <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/B-Flat.svg/2000px-B-Flat.svg.png"/>', 'Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet,Lorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit amet,Lorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit amet,Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/B-Flat.svg/2000px-B-Flat.svg.png"/>', 4, [t[2], t[4]]),
    _q(0, 'Question 5', 'Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet', '<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/B-Flat.svg/2000px-B-Flat.svg.png"/>,<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/B-Flat.svg/2000px-B-Flat.svg.png"/>,<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/B-Flat.svg/2000px-B-Flat.svg.png"/>,<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/B-Flat.svg/2000px-B-Flat.svg.png"/>', 2, [t[1], t[3]]),
    _q(1, 'Question 6', 'This is the sixth question', 'A,B,C,D', 3, [t[2], t[5]]),
    _q(2, 'Question 7', 'This is the seventh question', 'A,B,C,D', 1, [t[1], t[2]])
  ];

  // 5 users
  var users = [
    _user('tony', sha1('tony')),
    _user('kenny', sha1('kenny')),
    _user('javita', sha1('javita')),
    _user('yuen', sha1('yuen')),
  ];


  // Insert all users at once
  model.User.create(users, function(err, _users) {
    if (err) handleError(err);

    // _users are now saved to DB and have _id

    // Replace the owner indexes by their _ids
    for (var i = 0; i < questions.length; i++) {
      var ownerIdx = questions[i].owner;
      questions[i].creator = _users[ownerIdx]._id;
    }

    // Insert all questions
    model.Question.create(questions, function(err, _questions) {
      if (err) handleError(err);

      // Success
      console.log(_users);
      console.log(_questions);
      mongoose.connection.close();
    });
  });
}

function _user(username, password) {
  return {
    username: username,
    password: password
  };
}

// function _item(ownerIdx, description, createdOn, tags) {
//   return {
//     owner: ownerIdx,
//     description: description,
//     createdOn: createdOn,
//     tags: tags
//   };
// }

function _q(ownerIdx, title, description, four_choices, answer, tags) {
  return {
    owner: ownerIdx,
    title: title,
    description: description,
    four_choices: four_choices,
    answer: answer,
    tags: tags
  };
}

function handleError(err) {
  console.log(err);
  mongoose.connection.close();
}
