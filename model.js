var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId;


var UserSchema = Schema({
  username: { type: String, required: true, unique: true, maxlength: 12 },
  password: { type: String, required: true }
});


var QuestionSchema = Schema({
  title: {type: String, default: ''},
  description: { type: String, default: '' },
  four_choices: { type: String, default: ''},
  answer: {type: Number },
  tags: [ {type: String} ],
  creator: { type: ObjectId, ref: 'User' },
  last_modified_time: { type: Date, default: Date.now },
});


var User = mongoose.model('User', UserSchema);
var Question = mongoose.model('Question', QuestionSchema);

module.exports = {
  User: User,
  Question: Question
}

