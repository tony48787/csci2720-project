//Define utility function here
//All the function would be exported to index.js
var model = require("./model.js");
var jwt = require("jwt-simple");
var mongoose = require("mongoose");
var sanitizeHtml = require("sanitize-html");
var secret = "winnie";
var sha1 = require("sha1");

function authenticate(username, password, cb){
	model.User.findOne({
		username: username
	}, function(err, target){
		if(err){
			cb(null, false);
		}

		if(target.password == sha1(password)){
			cb(null, true);
		} else {
			cb(null, false);
		}
	});
}

function encodeToken(data){
	var token = jwt.encode(data, secret);
	return token;
}

function decodeToken(token){
	var data = jwt.decode(token, secret);
	return data;
}

function toObjectID(str){
	return mongoose.Types.ObjectId(str);
}

function checkAnswer(answer){
	if(typeof answer == "number"){
		if(answer < 1 || answer > 4){
			return false;
		} else {
			return answer;
		}
	} else {
		var intVal = parseInt(answer);
		if(isNaN(intVal)){
			return false;
		} else {
			if(answer < 1 || answer > 4){
				return false;
			} else {
				return intVal;
			}
		}
	}
}

function splitTags(tags){
	if(tags == undefined){
		return []
	} else if(tags.indexOf(",") == -1){
		return [tags];
	} else {
		return tags.split(",")
	}
}

function sanitizeInput(text){
	var noImgSrcFlag = false;
	var clean = sanitizeHtml(text, {
	  allowedTags: [ 'b', 'i', 'u', 'pre', 'p', 'br', 'img'],
	  allowedAttributes: {
	    'img': ['src', 'width', 'height']
	  },
	  transformTags: {
	  	'img': function(tagName, attribs){
	  		if(attribs.src == undefined){
	  			noImgSrcFlag = true;
	  		}
	  		if(attribs.width == undefined){
	  			attribs.width = "100";
	  		}
	  		if(attribs.height == undefined){
	  			attribs.height = "100";
	  		}
	  		return {
            tagName: 'img',
            attribs: attribs
        };
	  	}
	  }
	});

	if(noImgSrcFlag){
		return true;
	} else {
		return clean;
	}
}

function checkFourChoices(four_choices){
	if(four_choices.indexOf(",") == -1){
		return false;
	} else {
		var arr = four_choices.split(",");
		if(arr.length != 4){
			return false;
		} else {
			var notNull = true;
			arr.forEach(function(item){
				var strip_item = item.replace(/^\s+|\s+$/g, '');
				if(strip_item.length == 0){
					notNull = false;
				}
			})
			return notNull;
		}
	}
}

function trim(arr){
	var newArr = [];
	for(var i = 0; i < arr.length; i++){
		newArr[i] = arr[i].replace(/^\s+|\s+$/g, '');
	}
	return newArr;
}

function formatDateString(dateString){
	var date = new Date(dateString);
	var a = date.toDateString().split(" ");
	var b = date.toTimeString().split(" ")[0];
	b = b.split(":");
	var formatted = b[0] + ":" + b[1] + " " + a[1] + " " + a[2];
	return formatted;
}

function formatDescription(description){
	var len = 30;
	if(description.length < 30){
		return description;
	} else {
		return description.substr(0, len) + "...";
	}
}

module.exports = {
	authenticate: authenticate,
	encodeToken: encodeToken,
	decodeToken: decodeToken,
	toObjectID: toObjectID,
	sanitizeInput: sanitizeInput,
	checkAnswer: checkAnswer,
	splitTags: splitTags,
	checkFourChoices: checkFourChoices,
	formatDateString: formatDateString,
	formatDescription: formatDescription,
	trim: trim
};