var express = require("express");
var cookieParser = require("cookie-parser");
var bodyParser = require("body-parser");
var pug = require("pug");
var app = express();
var util = require("./util.js");
var async = require("async");
var json2csv = require("json2csv");


//Connect to Database
require("./db.js");

//Model of database
var model = require("./model.js");

app.set('views', __dirname + '/views');
app.set('view engine', 'pug');
//User Middleware
app.use(cookieParser());

app.get("/member", function(req, res){
	//Verify user details
	//Get user details
	var user = {};
	var _id = req.params.id;
	if(req.cookies.token){
		user = util.decodeToken(req.cookies.token);
	}
	res.render("member", {
		username: user.username
	});
});

/* Login */
app.get("/login", function(req, res){
	//res.render("login");
	var username = req.cookies.username;
	res.render("login", {
		username: username
	});
});

app.post("/login", bodyParser.urlencoded({extended: false}));

app.post("/login", function(req, res){
	var username = req.body.username;
	var password = req.body.password;
	async.series([
		function(cb){
			util.authenticate(username, password, cb)
		}
	], function(err, result){
		if(result[0]){
			//Success

			//Use jwt to encode the data
			model.User.findOne({
				username: username
			}, function(err, result){
				if(err){
					res.render("msg", {
						username: null,
						msg: "Datebase Error"
					});
					return;
				}
				var token = util.encodeToken({
					username: result.username,
					_id: result._id
				});
				res.cookie("token", token);
				res.redirect("/mcview");
			});

		} else {
			//Fail
			var msg = "Your username / password is incorrect. Please try again.";
			res.render("login", {
				username: null,
				msg: msg
			});
		}
	});

});
/* Login */



/* Logout */
app.get("/logout", function(req, res){
	res.clearCookie("token");
	res.redirect("/mcview");
});
/* Logout */

/* Multiple Choice List View */
app.get("/mcview", function(req, res){
	//Check if user is logged in

	var user = {};
	if(req.cookies.token){
		user = util.decodeToken(req.cookies.token);
	}

	var searchKey = req.query.searchKey;
	var searchValue = req.query.searchValue;
	var criteria = {};
	if(searchKey == "username"){
		model.User.findOne({
			username: searchValue
		}, function(err, user){
			if(err){
				res.render("msg", {
					username: null,
					msg: "Datebase Error"
				});
				return;
			}
			if(user){
				criteria["creator"] = user._id;
			} else {
				criteria["creator"] = null;
			}
			renderPage();
		});
	} else if(searchKey == "tags"){
		if(searchValue.indexOf(",") != -1){
			criteria["tags"] = {
				$all : searchValue.split(",")
			}
		} else {
			criteria["tags"] = searchValue;
		}
		renderPage();
	} else {
		renderPage();
	}

	function renderPage(){
		model.Question.find(criteria)
		.populate("creator")
		.sort({"last_modified_time": "asc"})
		.exec(function(err, questions){
			if(err){
				res.render("msg", {
					username: user.username,
					msg: "Datebase Error"
				});
				return;
			}

			//Pagination
			if(questions){

					//Date And Description formatting
				for(var i = 0; i < questions.length; i++){
					questions[i].formatted_last_modified_time = util.formatDateString(questions[i].last_modified_time);
					questions[i].brief_description = util.formatDescription(questions[i].description);
				};

				var pageSize = 5;
				var pageTotal = Math.ceil(questions.length / pageSize);
				var currentPage = req.query.page || 1;
				var filtered_questions = questions.slice((currentPage - 1) * pageSize, (currentPage - 1) * pageSize + pageSize);


				res.render("mcview", {
					username: user.username,
					questions: filtered_questions,
					pageTotal: pageTotal,
					pageSize: pageSize,
					currentPage: currentPage,
					searchKey: searchKey,
					searchValue: searchValue
				});
			} else {
				res.render("mcview", {
					username: user.username,
					questions: [],
					pageTotal: 0,
					pageSize: 0,
					currentPage: 0,
					searchKey: searchKey,
					searchValue: searchValue
				});
			}
			
		});
	}
	
});
/* Multiple Choice List View */

/* Multiple Choice Specific View */
app.get("/mcview/:id", function(req, res){
	var id = req.params.id;


	//Get user details
	var user = {};
	if(req.cookies.token){
		user = util.decodeToken(req.cookies.token);
	}

	//Find the question details from the id
	model.Question.findOne({
		_id: id
	})
	.populate("creator")
	.exec(function(err, question){
		if(err){
			res.render("msg", {
				username: user.username,
				msg: "Datebase Error"
			});
			return;
		}

		if(!question){
			res.render("msg", {
				username: user.username,
				msg: "This question does not exist anymore."
			});
			return;
		}

		question.formatted_last_modified_time = util.formatDateString(question.last_modified_time);
		if(question.tags.indexOf(",") != -1){
			question.tags = question.tags.join(",");
		}
		
		res.render("detailview", {
			username: user.username,
			question: question
		});
	});

});
/* Multiple Choice Specific View */

/* CRUD operations on MC questions */
app.get("/create", function(req, res){
	//Verify user details
	var user = {};
	var _id = req.params.id;

	if(req.cookies.token){
		user = util.decodeToken(req.cookies.token);
	}

	if(user.username){
		res.render("create", {
			username: user.username
		});
	} else {
		//Not Valid
		res.redirect("/login");
	}
});

app.post("/create", bodyParser.urlencoded({extended: false}));
app.post("/create", function(req, res){
	//Verify user details
	var user = {};
	var _id = req.params.id;

	if(req.cookies.token){
		user = util.decodeToken(req.cookies.token);
	}

	if(user.username){
		//Valid

		//Sanitize All Input Here

		var description = util.sanitizeInput(req.body.description);
		if(description === true){
			res.render("create",{
				username: user.username,
				question: req.body,
				msg: "There are at least 1 <img> in Your Description that do not contain src attribute."
			});
			return;
		}

		if(description.length == 0){
			res.render("create",{
				username: user.username,
				question: req.body,
				msg: "Your Description does not have any valid content."
			});
			return;
		}

		var four_choices = util.sanitizeInput(req.body.four_choices);
		if(four_choices === true){
			res.render("create",{
				username: user.username,
				question: req.body,
				msg: "There are at least 1 <img> in Your Four Choices that do not contain src attribute."
			});
			return;
		}

		if(four_choices.length == 0){
			res.render("create",{
				username: user.username,
				question: req.body,
				msg: "Your Description does not have any valid content."
			});
			return;
		}

		//Check format of four_choices
		if(!util.checkFourChoices(four_choices)){
			res.render("create", {
				username: user.username,
				question: req.body,
				msg: "Your format of four choices is wrong. It must be in comma-separated form and should not be null in any of four choices."
			});
			return;
		}

		four_choices = util.trim(four_choices.split(","));

		var answer = util.checkAnswer(req.body.answer);
		if(answer === false){
			res.render("create",{
				username: user.username,
				question: req.body,
				msg: "Your Answer is not VALID."
			});
			return;
		}

		var tags = util.splitTags(req.body.tags);
		var creator = util.toObjectID(user._id);

		var question = {
			title: req.body.title,
			description: description,
			four_choices: four_choices,
			answer: req.body.answer,
			tags: tags,
			creator: creator
		}

		model.Question.create(question, function(err, question){
	    if(err){
	    	res.render("msg", {
					username: user.username,
					msg: "Datebase Error"
				});
				return;
	    } 

	    if(question){
	    	res.render("msg", {
		    	username: user.username,
		    	question: question,
		    	msg: "Create Successful"
		    });
	    } else {
	    	res.render("msg", {
		    	username: user.username,
		    	question: question,
		    	msg: "Create Failed"
		    });
	    }
	    
	  });

	} else {
		//Not Valid
		res.redirect("/login");
	}
});

app.get("/mcview/:id/edit", function(req, res){
	//Verify user details
	var user = {};
	var _id = req.params.id;

	if(req.cookies.token){
		user = util.decodeToken(req.cookies.token);
	}

	if(user.username){
		model.Question.findOne({
			_id: _id
		})
		.populate("creator")
		.exec(function(err, question){
			if(err){
				res.render("msg", {
					username: user.username,
					msg: "Datebase Error"
				});
				return;
			}

			if(!question){
				res.render("msg", {
					username: user.username,
					msg: "This question does not exist anymore."
				});
				return;
			}

			//Verify creator with username
			if(user.username != question.creator.username){
				res.render("msg", {
					username: user.username,
					msg: "You are not allowed to modify other people's questions!"
				});
			} else {
				res.render("edit", {
					username: user.username,
					question: question
				});
			}
		})
		
	} else {
		//Not Valid
		res.redirect("/login");
	}

});

app.post("/mcview/:id/edit", bodyParser.urlencoded({extended: false}));
app.post("/mcview/:id/edit", function(req, res){
	//Verify user details
	//Get user details
	var user = {};
	var _id = req.params.id;
	if(req.cookies.token){
		user = util.decodeToken(req.cookies.token);
	}

	if(user.username){
		//Verified

		//Sanitize All Input Here
		var description = util.sanitizeInput(req.body.description);
		if(description === true){
			res.render("edit",{
				username: user.username,
				question: req.body,
				msg: "There are at least 1 <img> in Your Description that do not contain src attribute."
			});
			return;
		}
		if(description.length == 0){
			res.render("edit",{
				username: user.username,
				question: req.body,
				msg: "Your Description does not have any valid content."
			});
			return;
		}

		var four_choices = util.sanitizeInput(req.body.four_choices);
		if(four_choices === true){
			res.render("edit",{
				username: user.username,
				question: req.body,
				msg: "There are at least 1 <img> in Your Four Choices that do not contain src attribute."
			});
			return;
		}

		if(four_choices.length == 0){
			res.render("edit",{
				username: user.username,
				question: req.body,
				msg: "Your Description does not have any valid content."
			});
			return;
		}

		//Check format of four_choices
		if(!util.checkFourChoices(four_choices)){
			res.render("edit", {
				username: user.username,
				question: req.body,
				msg: "Your format of four choices is wrong. It must be in comma-separated form and should not be null in any of four choices."
			});
			return;
		}

		four_choices = util.trim(four_choices.split(","));

		var answer = util.checkAnswer(req.body.answer);
		if(answer === false){
			res.render("edit",{
				username: user.username,
				question: req.body,
				msg: "Your Answer is not VALID."
			});
			return;
		}

		var tags = util.splitTags(req.body.tags);
		
		var q = {
			title: req.body.title,
			description: description,
			four_choices: four_choices,
			answer: req.body.answer,
			tags: tags,
			last_modified_time: Date.now()
		}

		model.Question.findOneAndUpdate({
			_id: _id
		}, {
			$set: q
		}, {new:true}, function(err, question){
			if(err){
				res.render("msg", {
					username: user.username,
					msg: "Datebase Error"
				});
				return;
			}

			if(question){
				//Redirect to detail view
				res.redirect("/mcview/" + question._id);
			} else {
				//Update Fail
				res.render("msg", {
					username: user.username,
					question: question,
					msg: "Update Failed"
				});
			}
		})
	}
});

app.get("/mcview/:id/delete", function(req, res){
	//Verify user details
	//Get user details
	var user = {};
	var _id = req.params.id;
	if(req.cookies.token){
		user = util.decodeToken(req.cookies.token);
	}

	if(user.username){
		//Verified

		//Verify creator with username
		model.Question.findOne({
			_id: _id
		})
		.populate("creator")
		.exec(function(err, question){
			if(err){

			}

			if(!question){
				res.render("msg", {
					username: user.username,
					msg: "This question does not exist anymore."
				});
				return;
			}

			if(user.username != question.creator.username){
				res.render("msg", {
					username: user.username,
					msg: "You are not allowed to modify other people's questions!"
				});
				return;
			} 

			model.Question.findOneAndRemove({
				_id: _id
			}, function(err, question){
				if(err){
					res.render("msg", {
						username: user.username,
						msg: "Datebase Error"
					});
					return;
				}

				if(question){
					//Successful
					res.render("msg", {
						username: user.username,
						question: question,
						msg: "Delete Successful"
					});
				} else {
					//Not exist
					//res.send("Fail " + question);
					res.render("msg", {
						username: user.username,
						question: question,
						msg: "Delete Failed"
					});
				}

			});
		});
			
	} else {
		//Not Valid
		res.redirect("/login");
	}


});
/* CRUD operations on MC questions */

/* Export CSV */
app.get("/export", function(req, res){
	//Verify user details
	//Get user details
	var user = {};
	if(req.cookies.token){
		user = util.decodeToken(req.cookies.token);
	}

	if(user.username){
		model.Question.find()
		.populate("creator")
		.sort({"last_modified_time": "asc"})
		.exec(function(err, questions){
			if(err){
				res.render("msg", {
					username: user.username,
					msg: "Datebase Error"
				});
				return;
			}

			for(var i = 0; i < questions.length; i++){
				questions[i].formatted_last_modified_time = util.formatDateString(questions[i].last_modified_time);
			}

			res.render("export", {
				username: user.username,
				questions: questions
			});
		});
	} else {
		res.redirect("/login");
	}
});

app.post("/export", bodyParser.urlencoded({extended: false}));
app.post("/export", function(req, res){
	//Prompt users to save instead of showing the file

	//Verify user details
	//Get user details
	var user = {};
	var _ids = req.body.ids;
	if(req.cookies.token){
		user = util.decodeToken(req.cookies.token);
	}

	if(user.username){

		if(!_ids || _ids.length == 0){
			res.render("msg", {
				username: user.username,
				msg: "You need to select at least one MC."
			});
			return;
		}

		model.Question.find({
			_id: {
				$in: _ids
			}
		})
		.populate("creator")
		.exec(function(err, questions){
			if(err){
				res.render("msg", {
					username: user.username,
					msg: "Datebase Error"
				});
				return;
			}
			//Prepare output information
			var final_questions = [];
			questions.forEach(function(question, i){
				final_questions[i] = {};
				final_questions[i].title = question.title;
				final_questions[i].description = question.description;
				final_questions[i].four_choices = question.four_choices;
				final_questions[i].answer = question.answer;
				
				final_questions[i].tags = question.tags.toString();
				final_questions[i].creator = question.creator.username;
				
				final_questions[i].last_modified_time = util.formatDateString(question.last_modified_time);
			});


			res.set("Content-Type", "text/csv");
			var fields = ["title", "description", "four_choices", "answer", "tags", "creator", "last_modified_time"];
			var csv = json2csv({data: final_questions, fields: fields});

			res.send(csv);

		});
		
	} else {
		//Not Valid
		res.redirect("/login");
	}
	
});

/* Export CSV */

/* Index page */
app.get("/", function(req, res){
	res.redirect("/mcview");
});
/* Index page */

/* static file */
app.use(express.static("public"));
/* static file */

app.listen(8081);
console.log("Listening on localhost:8081");
